function isFull(array: Array<any>, maxLength: number): boolean {
  return array.length && array.length >= maxLength;
}


function isNumber(str: string): boolean {
  return /^\d+$/.test(str);
}

export {isFull, isNumber};
