// Cigna Coding Interview
import Processor from "./processor";

function run() {
  console.log("Welcome to the Cigna Processor");
  let processor = new Processor();
  process.stdin.setEncoding('utf8');
  process.stdin.on('data', processor.receive);
  process.stdin.once('end', processor.processData);
}

run();
