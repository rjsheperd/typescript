import {isFull, isNumber} from './utils';

export default class Processor {
  public data: string = "";
  private sentences: Array<string> = [];
  private queries: Array<string> = [];
  private maxSentences: number;
  private maxQueries: number;

  constructor() {};

  receive(chunk: string) {
    this.data += chunk;
  }

  processData() {
    // Split on new lines and process each line
    let lines = this.data.split("\n");
    lines.forEach(this.processLine);
    this.getResults();
  }

  processLine(line: string) {
    if (isNumber(line)) {
      let num = Number(line);
      this.setMaxSentences(num);
      this.setMaxQueries(num);
      return;
    }

    if (!isFull(this.sentences, this.maxSentences)) {
      this.sentences.push(line);
    } else if (!isFull(this.queries, this.maxQueries)) {
      this.queries.push(line);
    }
  }

  getResults() {
    let results = this.queries.map(this.numOfAppearances);
    console.log(results.join('\n'));
  }

  numOfAppearances(query: string): number {
    return this.sentences.reduce((acc, s) => acc + (s == query ? 1 : 0), 0);
  }

  setMaxSentences(max: number) {
    this.maxSentences = this.maxSentences || max;
  }

  setMaxQueries(max: number) {
    if (!isFull(this.sentences, this.maxSentences)) return;
    this.maxQueries = this.maxQueries || max;
  }
}
