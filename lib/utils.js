"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isFull(array, maxLength) {
    return array.length && array.length >= maxLength;
}
exports.isFull = isFull;
function isNumber(str) {
    return /^\d+$/.test(str);
}
exports.isNumber = isNumber;
