"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("./utils");
var Processor = (function () {
    function Processor() {
        this.data = "";
        this.sentences = [];
        this.queries = [];
    }
    ;
    Processor.prototype.receive = function (chunk) {
        this.data += chunk;
    };
    Processor.prototype.processData = function () {
        var lines = this.data.split("\n");
        lines.forEach(this.processLine);
        this.getResults();
    };
    Processor.prototype.processLine = function (line) {
        if (utils_1.isNumber(line)) {
            var num = Number(line);
            this.setMaxSentences(num);
            this.setMaxQueries(num);
            return;
        }
        if (!utils_1.isFull(this.sentences, this.maxSentences)) {
            this.sentences.push(line);
        }
        else if (!utils_1.isFull(this.queries, this.maxQueries)) {
            this.queries.push(line);
        }
    };
    Processor.prototype.getResults = function () {
        var results = this.queries.map(this.numOfAppearances);
        console.log(results.join('\n'));
    };
    Processor.prototype.numOfAppearances = function (query) {
        return this.sentences.reduce(function (acc, s) { return acc + (s == query ? 1 : 0); }, 0);
    };
    Processor.prototype.setMaxSentences = function (max) {
        this.maxSentences = this.maxSentences || max;
    };
    Processor.prototype.setMaxQueries = function (max) {
        if (!utils_1.isFull(this.sentences, this.maxSentences))
            return;
        this.maxQueries = this.maxQueries || max;
    };
    return Processor;
}());
exports.default = Processor;
