"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var processor_1 = require("./processor");
function run() {
    console.log("Welcome to the Cigna Processor");
    var processor = new processor_1.default();
    process.stdin.setEncoding('utf8');
    process.stdin.on('data', processor.receive);
    process.stdin.once('end', processor.processData);
}
run();
